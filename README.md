### Projet de synthèse Gitlab

Nécessite python 3+

## Usage
- Changez le fichier yaml pour y mettre votre token (et votre url github si vous ne le faites pas tourner en local)
- Faites : pip install -r requirements.txt dans le répertoire du projet
- Lancez le fichier ex1.py avec python et laissez tourner
- Enjoy !

## 
- Tentez de vous assigner des issues pour remplir votre to-do list. Toutes les minutes, dans votre terminal, vous avez les nouveaux todo.