#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Kev"

import gitlab
import yaml
import time

class GitLabApiTest:

    """
    Talk with your gitlab instance
    """    

    def __init__(self):

        # Get tocken to access gitlab account
        # format expected in token.yaml:   token_gitlab: gl*********************
        with open('token.yaml') as file:
            token = yaml.load(file, Loader=yaml.FullLoader)
            self.token = token['token_gitlab']
            self.url = token['url']

        #self.gl = gitlab.Gitlab('https://gitlab.com', private_token=self.token)
        self.gl = gitlab.Gitlab(self.url, private_token=self.token)
        self.todos = []


    def create_group(self, name):
        self.group_id = self.gl.groups.create({'name': name, 'path': name}).id           


    def check_group(self, name):
        return self.gl.groups.get(name)
    
    def create_group_project(self, name, group_id):
        self.project = self.gl.projects.create({'name': name, 'namespace_id': group_id})
        return self.project

    def create_user(self, email, password, username, name):
        self.user = self.gl.users.create({'email': email, 'password': password, 'username': username, 'name': name})
        return self.user
    
    def get_user(self, name):
        self.user = self.gl.users.list(username =  name)[0]
        return self.user
    
    def get_todos(self):
        self.todos = self.gl.todos.list()
        return self.todos
    
    def check_todos(self):
        todos = self.gl.todos.list()
        new_todos = list(set(self.todos) ^ set(todos))
        self.todos = todos
        return new_todos

if __name__ == "__main__":
    GLAT = GitLabApiTest()
    user = GLAT.get_user("cris3")
    if not user:
        user = GLAT.create_user("cris3@cris.fr", "azerty12345", "cris3", "cris3")
        i = 1
        while i <= 2:
            GLAT.create_group("Groupe_"+str(i))
            group = GLAT.check_group("Groupe_"+str(i))
            j = 0
            while j < 5:
                project = GLAT.create_group_project("Projet_"+str(i)+str(j), group.id)
                if i == 1 and j == 0:
                    k = 1
                    while k <= 10:
                        project.issues.create({'title': "Issue_"+str(k), 'description': "Description_"+str(k)})
                        k += 1
                if i == 1 and j % 2 != 0:
                    project.members.create({'user_id': user.id, 'access_level': gitlab.const.AccessLevel.DEVELOPER})
                elif i ==2 and j % 2 == 0:
                    project.members.create({'user_id': user.id, 'access_level': gitlab.const.AccessLevel.DEVELOPER})
                j += 1
            i += 1
    while True:
        print(GLAT.check_todos())
        time.sleep(60)


    
